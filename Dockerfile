FROM alpine:latest
RUN apk --no-cache add curl
RUN mkdir /var/resources
RUN mkdir /var/scripts
VOLUME /var/resources
VOLUME /var/scripts
ENTRYPOINT /var/scripts/entrypoint.sh

