#!/bin/sh

export BASE="http://conductorserver:8080/api"
echo "Waiting for conductor ..."
until $(curl --output /dev/null --silent --fail $BASE/health); do
    echo 'still waiting ...'
    sleep 5
done

echo 'Contact ... starting upload'

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/tasks/pytasks.json $BASE/metadata/taskdefs

#curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/group_deleted.json $BASE/metadata/workflow
