#!/bin/sh

export BASE="http://conductorserver:8080/api"
echo "Waiting for conductor ..."
until $(curl --output /dev/null --silent --fail $BASE/health); do
    echo 'still waiting ...'
    sleep 5
done

echo 'Contact ... starting upload'

# IAM

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/tasks/pytasks.json $BASE/metadata/taskdefs

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/user-group-role_created.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/user-group-role_deleted.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/user-group_created.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/user-group_deleted.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/group_created.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/group_deleted.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/invitation-accepted.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/portal/create-user-add-to-vre.json $BASE/metadata/workflow

# CCP
curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/ccp_demo/build-runtimes.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/ccp_demo/launch-runtimes.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/ccp_demo/clean-instances.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/ccp_demo/deploy-methods.json $BASE/metadata/workflow

curl -s -o /dev/null -w "%{http_code}\n" -X POST -H 'Content-Type: application/json' -d @/var/resources/workflows/ccp_demo/test-methods.json $BASE/metadata/workflow
