# gCube Orchestrator

**gCube Orchestrator** is a [Conductor](https://netflix.github.io/) based implementation of an orchestrator of administrative tasks.

It is based on a self-contained Docker image avilable on dockerhub that incorporates bith the Conductor server and UI.
The image currently uses an in-memory persistence and does not rely on any replication for performance or fault-tolerance.
Two Python based workers for executing REST calls and Python code have been added to support the needed functionality. 
They are pulled from dockerhub and initialized through the common docker-compose file together with their corresponding Task definitions.

In addition, the initialization phase also uploads a set of workflows that handle events coming from:

- the gCube Liferay based gateways 
- the IAM service based on Keycloak

Please refer to proper documentation for more information.

## Structure of the project

The docker-compose file pulls two public images from the nubisware account on [Docker hub](https://hub.docker.com/) which setup the conductor server and UI and the Python based workers. 
A third image for initializing the orchestrator is created on the fly from the Dockerfile.

## Built With

* [Docker](https://www.docker.com/) - v. 19.03+
* [docker-compose](https://docs.docker.com/compose) - v. 3.8+

## Documentation

The command to pull a complete instance of the gCube Orchestrator is

    docker-compose up -d

The command to tear down an instance of the gCube Orchestrator is
    
    docker-compose down

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/orchestrator-setup/releases).

## Authors

* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260);
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488), D4Science-II (grant no.239019), ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine(grant no. 283644);
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680), EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant no. 654119), SoBigData (grant no. 654024),DESIRA (grant no. 818194), ARIADNEplus (grant no. 823914), RISIS2 (grant no. 824091), PerformFish (grant no. 727610), AGINFRAplus (grant no. 731001);
    